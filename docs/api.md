# API Endpoints

## Groundstation

* index (fleet)
* register
* dispatch
* enqueue
* tasks

## Satellite

* index (healthcheck)
* run
* logs