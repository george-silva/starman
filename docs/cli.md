# CLI

All of the interaction between these can be done
using the CLI or the webapp. This is the CLI reference.

## Enqueue tasks

Use this command to enqueue tasks in the groundstation.
You first enqueue as many tasks as you'd like and then
dispatch them to the satellites.

```
./manage.py enqueue <task_name> [payoff] [resources]
```

**Example response**:

```
./manage.py enqueue lavar 1000 r5
{
    "status": "SUCCESS",
    "message": "Task lavar - 1000 enqueued.",
    "tasks": [
        {
            "name": "lavar",
            "resources": [
                "r5"
            ],
            "payoff": 1000
        }
    ]
}
```

In this case, you can list resources as string,
using commas to separate each resource used by
a task. `r1,r2` means that this task will
use the `r1` and `r2` resources.

Resources are optional. If they are not specified, this will
consider that all resources (`r1` to `r6`) will be used.

While you can use any string, by default, our
satellites can only handle resources `r1` to `r6`.

If you add a task with a resource that any satellites
do not possess, it will keep lingering on the `UNSCHEDULED`
schedule.

## Dispatch

Using `dispatch` allows you to delegate tasks to certain satellites,
based on the current running `TaskManager`.

The `TaskManager` can be something much more complex, to be honest,
but this is using a greedy version of it.

If the `TaskManager` decides that there are no executable tasks, they
will be listed on the `unscheduled` response key.

```
./manage.py dispatch
{
    "status": "SUCCESS",
    "message": "Tasks dispatched and executed.",
    "dispatched": [
        [
            {
                "satellite": "EGPGMLEL",
                "port": 5001,
                "datetime": "Mon, 18 Jun 2018 23:12:35 GMT",
                "duration": 6.866455078125e-05,
                "task": "lavar",
                "payoff": 1000,
                "resources": [
                    "r5"
                ],
                "success": false,
                "message": "Task lavar - 1000 was executed"
            }
        ],
        {
            "satellite": "KBYWZZWC",
            "port": 5002,
            "datetime": "Mon, 18 Jun 2018 23:12:35 GMT",
            "duration": 0,
            "success": true,
            "message": "No tasks were executed because schedule was empty."
        }
    ],
    "unscheduled": []
}
```

## Listing fleet

To list your fleet, you can use:

```
./manage.py fleet
{
    "status": "SUCCESS",
    "message": "GROUNDSTATION up and running.",
    "fleet": [
        {
            "name": "ONNNXUBO",
            "port": 5001,
            "address": "ONNNXUBO:5001"
        },
        {
            "name": "PFJAJXZS",
            "port": 5002,
            "address": "PFJAJXZS:5002"
        }
    ]
}
```

## Listing tasks

To list enqueued tasks, do:

```
./manage.py tasks
{
    "status": "SUCCESS",
    "message": "Fetched enqueued tasks.",
    "tasks": [
        {
            "name": "lavar",
            "resources": [
                "r5"
            ],
            "payoff": 1000
        }
    ]
}
```

## Healthcheck

You can check if a satellite is online with:

```
./manage.py healthcheck [port]
```

## Launch Satellites

To launch N satellites, do, with your virtualenv activated:

Each satellite in this case is represented by a simple
Flask application, each running on it's own port.

```
./manage.py launch_fleet 2
 * Serving Flask app "starman.satellite" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Serving Flask app "starman.satellite" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://127.0.0.1:5002/ (Press CTRL+C to quit)
 * Running on http://127.0.0.1:5001/ (Press CTRL+C to quit)
 ```

Optionally you can choose the number of satellites
you want to launch, specifying an integer argument

```
./manage.py launch_fleet 100
```

When you launch a series of satellites, each of them
will try to register themselves against the `groundstation`,
by hitting the `register` endpoint.

### Groundstation logs

You can collect ALL the satellites logs using

```
./manage.py groundlogs
```

This will go to the groundstation and proceed to
collect each log for each satellite and it will
return them all.

## Satellite Logs

You can get satellite logs using the logs command.
The port which the satellite is running is optional
and defaults to `5001`.

```
./manage.py logs [port]
[
    {
        "satellite": "EGPGMLEL",
        "port": 5001,
        "datetime": "Mon, 18 Jun 2018 23:12:35 GMT",
        "duration": 6.866455078125e-05,
        "task": "lavar",
        "payoff": 1000,
        "resources": [
            "r5"
        ],
        "success": false,
        "message": "Task lavar - 1000 was executed"
    }
]

```

## CLI Reference

* `./manage.py launch_fleet [10]`
* `./manage.py fleet`
* `./manage.py tasks`
* `./manage.py enqueue name [payoff] [resources]`
* `./manage.py clear_tasks`
* `./manage.py dispatch`
* `./manage.py groundlogs`
* `./manage.py logs [port]`
* `./manage.py healthcheck [port]`