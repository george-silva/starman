# Installation

## Backend

1. Create a virtualenv
2. Install the python dependencies with `pip -r requirements/tests.txt`
3. After installation, you'll need to run the services, described below.

## Frontend

1. Go to the `frontend` folder;
1. do `npm install`
1. do `npm run dev`
1. You'll be able to access the frontend over `localhost:8080`

## Testing

All the tests were written using `pytest`. If you want to
run the tests you need to have pytest installed in your virtualenv
(if you installed the tests.txt requirements file, you should be fine).

To run, just do `pytest` on the `backend` folder from your terminal.

There are no unit tests for the front-end application.

## Coverage

To track software code coverage, run, from the `backend` folder:

```
py.test --cov=starman tests/
```

## Running

All the procedures here described in this section assumes that
your virtualenv is activated.

Also, they assume that you are on the folder `backend`.

### Groundstation

```
FLASK_APP=starman.groundstation.py flask run
```

### Satellites

```
./manage.py launch fleet [N]
```

### Frontend

After installing all `npm` dependencies:

1. `npm run dev`
1. go to `localhost:8080`