import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    fleet: null,
    tasks: null,
    logs: null,
    error: null
  },
  getters: {
    fleet (state, getters) {
      return state.fleet
    },
    tasks (state, getters) {
      return state.tasks
    },
    logs (state, getters) {
      return state.logs
    }
  },
  mutations: {
    ADD_LOG (state, log) {
      if (!log) {
        return
      }
      if (!state.logs) {
        state.logs = [log]
      } else {
        state.logs = [
          log,
          ...state.logs
        ]
      }
    },
    ADD_TASK (state, task) {
      state.tasks = [
        ...state.tasks,
        task
      ]
    },
    SET_FLEET (state, fleet) {
      state.fleet = fleet
    },
    SET_TASKS (state, tasks) {
      state.tasks = tasks
    },
    SET_LOGS (state, logs) {
      state.logs = logs
    },
    SET_ERROR (state, error) {
      state.error = error
    }
  },
  actions: {
    FETCH_FLEET (context, payload) {
      return Axios.get('/').then((response) => {
        context.commit('SET_FLEET', response.data.fleet)
        return response.data.fleet
      })
    },
    FETCH_TASKS (context, payload) {
      return Axios.get('/tasks').then((response) => {
        context.commit('SET_TASKS', response.data.tasks)
        return response.data.tasks
      })
    },
    ADD_TASK (context, payload) {
      return Axios.post('/enqueue', payload).then((response) => {
        const task = {...response.data.task}
        context.commit('ADD_TASK', task)
        context.commit('ADD_LOG', response.data)
        return response.data.task
      })
    },
    CLEAR_TASKS (context, payload) {
      return Axios.post('/clear_tasks').then((response) => {
        context.commit('SET_TASKS', null)
        context.commit('ADD_LOG', response.data)
      })
    },
    DISPATCH_ALL (context, payload) {
      return Axios.post('/dispatch').then((response) => {
        context.commit('SET_TASKS', response.data.unscheduled)
        context.commit('ADD_LOG', response.data)
      })
    },
    FETCH_LOGS (context, payload) {
      return Axios.get('/logs').then((response) => {
        context.commit('SET_LOGS', response.data.logs)
        return response.data.logs
      })
    }
  }
})
