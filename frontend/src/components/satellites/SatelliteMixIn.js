export default {
  computed: {
    satelliteName () {
      if (!this.satellite) {
        return ''
      }
      return `${this.satellite.name}:${this.satellite.port}`
    }
  }
}
