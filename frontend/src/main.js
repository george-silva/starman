// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Axios from 'axios'
import TreeView from 'vue-json-tree-view'
Vue.use(TreeView)
Vue.config.productionTip = false

// could be process.env.BASE_URL, for example
Axios.defaults.baseURL = 'http://localhost:5000'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
