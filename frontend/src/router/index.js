import Vue from 'vue'
import Router from 'vue-router'
import DashboardPage from '@/pages/dashboard/DashboardPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: DashboardPage
    }
  ]
})
