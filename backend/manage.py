#!/usr/bin/env python
import click
import json
import time
import requests
from threading import Thread


@click.group()
def cli():
    pass


def run_satellite(port):
    from starman.satellite import create_app
    app = create_app(port)
    app.run(debug=True, use_reloader=False, port=port)


def launch_satellite(port):
    port = int(port)
    t = Thread(name='t', target=run_satellite, args=[port])
    t.setDaemon(True)
    t.start()


@click.command()
def tasks():
    response = requests.get('http://localhost:5000/tasks')
    tasks = response.json()
    click.secho(json.dumps(tasks, indent=4), bold=True)
    click.secho('TASKS REQUEST DONE', fg='green', bold=True)


@click.command()
def fleet():
    response = requests.get('http://localhost:5000/')
    fleet = response.json()
    click.secho(json.dumps(fleet, indent=4), bold=True)
    click.secho('FLEET REQUEST DONE', fg='green', bold=True)


@click.command()
@click.argument('name')
@click.argument('payoff', default=1)
@click.argument('resources', default='r1,r2,r3,r4,r5,r6')
def enqueue(name, payoff, resources):
    response = requests.post('http://localhost:5000/enqueue', json={
        'name': name,
        'payoff': payoff,
        'resources': resources
    })
    tasks = response.json()
    click.secho(json.dumps(tasks, indent=4), bold=True)
    click.secho('TASK ADDED TO GROUNDSTATION', fg='green', bold=True)


@click.command()
def clear_tasks():
    response = requests.post('http://localhost:5000/clear_tasks')
    data = response.json()
    click.secho(json.dumps(data, indent=4), bold=True)
    click.secho('TASKS CLEARED', fg='green', bold=True)


@click.command()
def dispatch():

    response = requests.post('http://localhost:5000/dispatch')
    schedules = response.json()
    click.secho(json.dumps(schedules, indent=4), bold=True)
    click.secho('TASKS DISPATCHED. IMPOSSIBLE TASKS TO DISPATCH READDED TO GROUNDSTATION', fg='green', bold=True)


@click.command()
@click.argument('n', default=10)
def launch_fleet(n):

    base_port = 5001
    n = int(n)
    for i in range(base_port, base_port + n):
        launch_satellite(i)
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print('done')
        exit(0)


@click.command()
@click.argument('port', default=5001)
def healthcheck(port):

    response = requests.get(f'http://localhost:{port}')
    logs = response.json()
    click.secho(json.dumps(logs, indent=4), bold=True)
    click.secho(f'SATELLITE {port} HEALTHCHECK SUCCESSFUL.', bold=True, fg='green')


@click.command()
@click.argument('port', default=5001)
def logs(port):

    response = requests.get(f'http://localhost:{port}/logs')
    logs = response.json()
    click.secho(json.dumps(logs, indent=4), bold=True)
    click.secho(f'FETCHED SATELLITE {port} LOGS SUCCESSFULLY.', bold=True, fg='green')


@click.command()
def groundlogs():
    response = requests.get(f'http://localhost:5000/logs')
    logs = response.json()
    click.secho(json.dumps(logs, indent=4), bold=True)
    click.secho(f'FETCHED ALL SATELLITE LOGS SUCCESSFULLY.', bold=True, fg='green')

cli.add_command(enqueue)
cli.add_command(clear_tasks)
cli.add_command(dispatch)
cli.add_command(tasks)
cli.add_command(fleet)
cli.add_command(logs)
cli.add_command(groundlogs)
cli.add_command(healthcheck)
cli.add_command(launch_fleet)

if __name__ == '__main__':
    cli()
