# coding: utf-8
import pytest
from starman.models import (
    Satellite,
    Task,
)
from starman.tasks import (
    TaskManager,
    GreedyTaskManager,
)


def test_task_manager_can_init():

    task_manager = TaskManager()
    assert task_manager is not None


def test_task_manager_can_get_tasks():

    task_manager = TaskManager()
    tasks = list()
    output = task_manager.get_tasks(tasks)
    assert len(output) == 0
    assert isinstance(output, list)


def test_task_manager_orders_get_tasks():
    task_manager = TaskManager()
    tasks = [
        Task(name='foo', payoff=1, resources=('r1', 'r2',)),
        Task(name='bar', payoff=2, resources=('r1', 'r2',)),

    ]
    output = task_manager.get_tasks(tasks)
    assert len(output) == 2
    assert output[0].name == 'foo'


def test_task_manager_orders_get_tasks_three_tasks():
    task_manager = TaskManager()
    tasks = [
        Task(name='baz', payoff=100, resources=('r1', 'r2',)),
        Task(name='foo', payoff=1, resources=('r1', 'r2',)),
        Task(name='bar', payoff=2, resources=('r1', 'r2',)),

    ]
    output = task_manager.get_tasks(tasks)
    assert len(output) == 3
    assert output[0].name == 'foo'
    assert output[1].name == 'bar'
    assert output[2].name == 'baz'


def test_task_manager_raises_dispatch():
    task_manager = TaskManager()
    with pytest.raises(NotImplementedError):
        task_manager.dispatch(fleet=list(), tasks=list())


def test_greedy_task_manager_can_init():

    task_manager = GreedyTaskManager()
    assert task_manager is not None


def test_greedy_task_manager_returns_list_of_schedules():

    sat1 = Satellite(name='sat1')
    sat2 = Satellite(name='sat2')
    task1 = Task(name='foo', payoff=1, resources=('r1', 'r2',))
    task2 = Task(name='bar', payoff=1, resources=('r1', 'r2',))
    task_manager = GreedyTaskManager()
    result = task_manager.dispatch(
        fleet=[sat1, sat2],
        tasks=[task1, task2]
    )
    assert len(result) == 3  # count the unscheduled
    assert isinstance(result, list)


def test_greedy_task_manager_assigns_to_all_satellites():

    sat1 = Satellite(name='sat1')
    sat2 = Satellite(name='sat2')
    task1 = Task(name='foo', payoff=1, resources=('r1', 'r2',))
    task2 = Task(name='bar', payoff=1, resources=('r1', 'r2',))
    task_manager = GreedyTaskManager()
    result = task_manager.dispatch(
        fleet=[sat1, sat2],
        tasks=[task1, task2]
    )
    impossible_schedule = list([r for r in result if r.unscheduled])
    possible_schedule = list([r for r in result if not r.unscheduled])
    assert len(impossible_schedule) == 1
    assert len(possible_schedule) == 2
    impossible_schedule = impossible_schedule[0]
    assert len(impossible_schedule.tasks) == 0
    for s in possible_schedule:
        assert len(s.tasks) == 1

    one = possible_schedule[0]
    two = possible_schedule[1]

    assert one.satellite.name == 'sat1'
    assert two.satellite.name == 'sat2'
    assert one.tasks[0].name == 'bar'
    assert two.tasks[0].name == 'foo'


def test_greedy_task_manager_does_not_assign_impossible():
    sat1 = Satellite(name='sat1')
    sat2 = Satellite(name='sat2')
    task1 = Task(name='abc', payoff=1, resources=('r1', 'r2',))
    task2 = Task(name='def', payoff=1, resources=('r1', 'r2',))
    task3 = Task(name='ghi', payoff=1, resources=('r1', 'r2', 'r3'))
    task_manager = GreedyTaskManager()
    result = task_manager.dispatch(
        fleet=[sat1, sat2],
        tasks=[task2, task1, task3]
    )
    impossible_schedule = list([r for r in result if r.unscheduled])
    impossible_schedule = impossible_schedule[0]
    imp_tasks = [t.name for t in impossible_schedule.tasks]
    assert len(impossible_schedule.tasks) == 1
    assert 'ghi' in imp_tasks


def test_greedy_task_manager_assigns_multiple_tasks():
    sat1 = Satellite(name='sat1')
    sat2 = Satellite(name='sat2')
    # sat1
    task1 = Task(name='foo', payoff=1, resources=('r1', 'r2',))
    # sat2
    task2 = Task(name='bar', payoff=1, resources=('r1', 'r2',))
    # sat1
    task3 = Task(name='baz', payoff=1, resources=('r3',))
    task_manager = GreedyTaskManager()
    result = task_manager.dispatch(
        fleet=[sat1, sat2],
        tasks=[task1, task2, task3]
    )
    possible_schedule = list([r for r in result if not r.unscheduled])
    one = possible_schedule[0]
    two = possible_schedule[1]
    one_tasks = [t.name for t in one.tasks]
    two_tasks = [t.name for t in two.tasks]

    assert len(one.tasks) == 2
    assert len(two.tasks) == 1
    # this test proves the ordering. we are prioritizing
    # not just the payoff, but also the number of resources
    # used.
    assert 'bar' in two_tasks
    assert 'baz' in one_tasks
    assert 'foo' in one_tasks


def test_greedy_manager_three_tasks():
    sat1 = Satellite(name='sat1')
    sat2 = Satellite(name='sat2')
    task1 = Task(name='foo', payoff=10, resources=('r1',))
    task2 = Task(name='bar', payoff=6, resources=('r1',))
    task3 = Task(name='baz', payoff=3, resources=('r1',))
    task_manager = GreedyTaskManager()
    result = task_manager.dispatch(
        fleet=[sat1, sat2],
        tasks=[task2, task3, task1]
    )
    possible_schedule = list([r for r in result if not r.unscheduled])
    impossible_schedule = list([r for r in result if r.unscheduled])[0]
    one = possible_schedule[0]
    two = possible_schedule[1]
    one_tasks = [t.name for t in one.tasks]
    two_tasks = [t.name for t in two.tasks]
    imp_tasks = [t.name for t in impossible_schedule.tasks]

    assert len(one.tasks) == 1
    assert len(two.tasks) == 1
    assert len(impossible_schedule.tasks) == 1
    assert 'foo' in one_tasks
    assert 'bar' in two_tasks
    assert 'baz' in imp_tasks


def test_greedy_manager_canonical_test():
    sat1 = Satellite(name='sat1')
    sat2 = Satellite(name='sat2')
    task1 = Task(name='foo', payoff=10, resources=('r1', 'r5',))
    task2 = Task(name='bar', payoff=1, resources=('r1', 'r2',))
    task3 = Task(name='baz', payoff=1, resources=('r5', 'r6',))
    task4 = Task(name='imp', payoff=0.1, resources=('r1', 'r6',))
    task_manager = GreedyTaskManager()
    result = task_manager.dispatch(
        fleet=[sat1, sat2],
        tasks=[task1, task2, task3, task4]
    )
    possible_schedule = list([r for r in result if not r.unscheduled])
    impossible_schedule = list([r for r in result if r.unscheduled])[0]
    one = possible_schedule[0]
    two = possible_schedule[1]
    one_tasks = [t.name for t in one.tasks]
    two_tasks = [t.name for t in two.tasks]
    imp_tasks = [t.name for t in impossible_schedule.tasks]

    assert len(one.tasks) == 1
    assert len(two.tasks) == 2
    assert 'foo' in one_tasks
    assert 'bar' in two_tasks
    assert 'baz' in two_tasks
    assert 'imp' in imp_tasks
