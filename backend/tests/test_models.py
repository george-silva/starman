# coding: utf-8
from unittest import mock
from unittest.mock import call
from starman.models import (
    Groundstation,
    Satellite,
    Task,
    Schedule,
)


def test_init_groundstation():

    g = Groundstation()
    assert g is not None


def test_init_groundstation_has_fleet():

    g = Groundstation()
    assert g.fleet is not None
    assert isinstance(g.fleet, list)
    assert len(g.fleet) == 0


def test_init_groundsation_has_tasks():
    g = Groundstation()
    assert g.tasks is not None
    assert isinstance(g.tasks, list)
    assert len(g.tasks) == 0


def test_logs_calls_inner_log_for_each_satellite():

    g = Groundstation()
    sat1 = Satellite(name='foo')
    sat2 = Satellite(name='bar')
    g.fleet = [sat1, sat2]
    g._logs = mock.MagicMock(name='_logs', return_value=dict())
    result = g.logs()
    assert isinstance(result, list)
    assert len(result) == 2
    expected = [call(sat1), call(sat2)]
    assert g._logs.call_args_list == expected


def test_dispatch_calls_inner_dispatch_for_each_schedule():

    g = Groundstation()
    sat1 = Satellite(name='foo')
    sch1 = Schedule(sat1)
    t1 = Task(name='t1', resources=('r1', ))
    sch1.schedule(t1)
    sat2 = Satellite(name='bar')
    sch2 = Schedule(sat2)
    t2 = Task(name='t2', resources=('r1', ))
    sch2.schedule(t2)
    schedules = [sch1, sch2]
    g.fleet = [sat1, sat2]
    g._dispatch = mock.MagicMock(name='_dispatch', return_value=dict())
    result = g.dispatch(*schedules)
    assert isinstance(result, list)
    assert len(result) == 2
    expected = [call(sch1), call(sch2)]
    assert g._dispatch.call_args_list == expected


def test_init_satellite_work():

    sat = Satellite(name='foo')
    assert sat is not None
    assert len(sat.resources) == 6
    assert sat.name == 'foo'
    assert sat.port == 5001
    assert sat.failure_rate == 0.1
    assert sat.logs is not None
    assert len(sat.logs) == 0


def test_satellite_to_string():

    sat = Satellite(name='foo')
    string_representation = str(sat)
    assert string_representation == 'foo:5001'


def test_satellite_to_json():

    sat = Satellite(name='foo')
    json = {
        'name': 'foo',
        'port': 5001,
        'address': 'foo:5001'
    }
    assert sat.to_json == json


def test_satellite_inner_run_is_not_called_without_tasks():

    sat = Satellite(name='foo')
    sat._run = mock.MagicMock(name='_run', return_value=dict())
    tasks = list()
    sat.run(*tasks)
    assert not sat._run.called


def test_satellite_inner_run_is_called():

    sat = Satellite(name='foo')
    sat._run = mock.MagicMock(name='_run', return_value=dict())
    tasks = ['t1', 't2']
    result = sat.run(*tasks)
    expected = [call('t1'), call('t2')]
    assert sat._run.call_args_list == expected
    assert len(result) == 2


def test_satellite_log_task():
    sat = Satellite(name='foo')
    assert len(sat.logs) == 0
    task = Task(name='t1', resources=('r1', ))
    log = sat.log_task(task, 1)
    assert len(sat.logs) == 1
    assert log is not None
    assert log['status'] == 'SUCCESS'
    assert log['message'] == f'TASK {task} was executed with SUCCESS.'
    assert log['task'] == 't1'
    assert log['duration'] == 1
    assert log['payoff'] == 1
    assert log['resources'] == ('r1', )


def test_satellite_log_task_with_message():
    sat = Satellite(name='foo')
    assert len(sat.logs) == 0
    task = Task(name='t1', resources=('r1', ))
    log = sat.log_task(task, 1, message='hello world')
    assert len(sat.logs) == 1
    assert log is not None
    assert log['status'] == 'SUCCESS'
    assert log['message'] == f'hello world'
    assert log['task'] == 't1'
    assert log['duration'] == 1
    assert log['payoff'] == 1
    assert log['resources'] == ('r1', )


def test_task_init():

    t = Task(name='foo', resources=('r1', ))
    assert t is not None
    assert t.name == 'foo'
    assert t.resources == ('r1', )
    assert t.payoff == 1
    assert t.duration >= 1 and t.duration <= 10
    assert t.execute_at is None


def test_task_lt_different_payoffs():

    t1 = Task(name='foo', payoff=2, resources=('r1', ))
    t2 = Task(name='bar', payoff=1, resources=('r1', ))
    assert t2 < t1


def test_task_lt_equal_payoffs_different_resources():

    t1 = Task(name='foo', payoff=2, resources=('r1', ))
    t2 = Task(name='bar', payoff=2, resources=('r1', 'r2', ))
    assert t2 < t1


def test_task_to_str():

    t1 = Task(name='foo', payoff=2, resources=('r1', ))
    string_representation = str(t1)
    assert string_representation == 'foo - 2'


def test_task_to_repr():

    t1 = Task(name='foo', payoff=2, resources=('r1', ))
    reprs = repr(t1)
    assert reprs == "Task(name='foo', resources=('r1',), payoff=2)"


def test_task_to_json():

    t1 = Task(name='foo', payoff=2, resources=('r1', ))
    json = {
        'name': 'foo',
        'resources': ('r1', ),
        'payoff': 2
    }
    assert t1.to_json == json


def test_schedule_init():

    sat1 = Satellite(name='foo')
    schedule = Schedule(sat1)
    assert schedule is not None
    assert schedule.satellite == sat1
    assert isinstance(schedule.tasks, list)
    assert len(schedule.tasks) == 0


def test_is_scheduled():
    schedule = Schedule()
    assert schedule.unscheduled


def test_schedule_should_add_task_to_list():

    schedule = Schedule()
    task = Task(name='foo', resources=('r1', ))
    schedule.schedule(task)
    assert len(schedule.tasks) == 1


def test_schedule_used_resources_returns_without_tasks_returns_empty_set():

    schedule = Schedule()
    used_resources = schedule.used_resources()
    assert isinstance(used_resources, set)
    assert len(used_resources) == 0


def test_schedule_used_resources_returns_task_resources():

    schedule = Schedule()
    task = Task(name='foo', resources=('r1', ))
    schedule.schedule(task)
    used_resources = schedule.used_resources()
    assert isinstance(used_resources, set)
    assert len(used_resources) == 1
    assert used_resources == set(['r1'])


def test_can_schedule_with_different_resource():
    sat = Satellite(name='sat1')
    schedule = Schedule(sat)
    task1 = Task(name='foo', resources=('r1', ))
    schedule.schedule(task1)
    task2 = Task(name='bar', resources=('r2', ))
    assert schedule.can_schedule(task2)


def test_cannot_schedule_with_same_resource():
    sat = Satellite(name='sat1')
    schedule = Schedule(sat)
    task1 = Task(name='foo', resources=('r1', ))
    schedule.schedule(task1)
    task2 = Task(name='bar', resources=('r1', ))
    assert not schedule.can_schedule(task2)


def test_cannot_schedule_with_resource_that_does_not_exist():
    sat = Satellite(name='sat1')
    schedule = Schedule(sat)
    task1 = Task(name='foo', resources=('r1', ))
    schedule.schedule(task1)
    task2 = Task(name='bar', resources=('resource-does-not-exist', ))
    assert not schedule.can_schedule(task2)


def test_cannot_schedule_without_satellite():
    sat = Satellite(name='sat1')
    schedule = Schedule()
    task2 = Task(name='bar', resources=('r1', ))
    assert not schedule.can_schedule(task2)


def test_schedule_to_json_no_tasks_no_sat():
    schedule = Schedule()
    json = {
        'name': 'UNSCHEDULED',
        'type': 'IMMEDIATE',
        'tasks': list()
    }

    assert schedule.to_json == json


def test_schedule_to_json_no_tasks():
    schedule = Schedule(Satellite(name='foo'))
    json = {
        'name': 'foo:5001',
        'type': 'IMMEDIATE',
        'tasks': list()
    }

    assert schedule.to_json == json


def test_schedule_to_json():
    schedule = Schedule(Satellite(name='foo'))
    task = Task(name='t1', resources=('r1', ))
    schedule.schedule(task)
    json = {
        'name': 'foo:5001',
        'type': 'IMMEDIATE',
        'tasks': [
            {
                'name': 't1',
                'payoff': 1,
                'resources': ('r1', )
            }
        ]
    }

    assert schedule.to_json == json
