# coding: utf-8
from itertools import cycle
from .models import (
    Schedule
)


class TaskManager(object):

    """
    TaskManager is an object responsible to
    dispatch and filter tasks.
    """

    def __init__(self):
        pass

    def get_tasks(self, tasks):
        """
        Returns a new list of tasks, so
        the manager can do it's thing to it.
        """
        return sorted(tasks)

    def dispatch(self, fleet, tasks):
        """
        Associates tasks with satellites, using
        your own algorithm.
        """
        raise NotImplementedError


class CSPTaskManager(TaskManager):

    def dispatch(self, fleet, tasks):
        """
        Use ortools to solve this problem.
        """
        raise NotImplementedError


class GreedyTaskManager(TaskManager):

    def dispatch(self, fleet, tasks):
        """
        Dispatches tasks, given a fleet
        and N tasks.
        This is a greedy implementation.
        Ideally this should be solved
        using CP, like python-constraint or
        or-tools.

        For each satellite we create a schedule.
        We cycle through each schedule, assigning
        a task in turn. First task to the first
        satellite, second task to the second, etc.

        If the task cannot be scheduled by any reason,
        it is readded to the popping_tasks list.
        If we visit all the satellites with a single
        task, it means it cannot be dispatched to any
        satellite in the fleet, therefore it's added
        to the UNSCHEDULED schedule.
        """
        schedules = [Schedule(sat) for sat in fleet]
        unscheduled = Schedule()
        visited_tasks = dict()
        popping_tasks = self.get_tasks(tasks)
        sat_count = len(fleet)
        for schedule in cycle(schedules):
            if len(popping_tasks) == 0:
                break
            task = popping_tasks.pop()
            if task in visited_tasks:
                visited_tasks[task].append(schedule)
            else:
                visited_tasks[task] = list([schedule])

            if schedule.can_schedule(task):
                schedule.schedule(task)
            else:
                # try to schedule this in
                # the next satellite
                if len(visited_tasks[task]) == sat_count:
                    unscheduled.schedule(task)
                else:
                    popping_tasks.append(task)
        schedules.append(unscheduled)
        return schedules
