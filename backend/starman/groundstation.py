# coding: utf-8
import copy
from flask import Flask, request, jsonify, render_template
from flask_cors import CORS
import wtforms_json
from .utils import StarmanJSONEncoder
from .forms import TaskForm
from .models import Satellite, Groundstation, Task
from .tasks import GreedyTaskManager
wtforms_json.init()
groundstation = Flask(__name__)
CORS(groundstation)
groundstation.json_encoder = StarmanJSONEncoder
groundstation.config['TEMPLATES_AUTO_RELOAD'] = True
groundstation.config['JSON_SORT_KEYS'] = False
GROUNDSTATION = Groundstation()


@groundstation.route('/')
def home():
    """
    returns fleet
    """
    global GROUNDSTATION
    fleet = copy.deepcopy(GROUNDSTATION.fleet)
    fleet = sorted(fleet, key=lambda s: s.port)
    return jsonify(
        status='SUCCESS',
        message='GROUNDSTATION up and running.',
        fleet=[s.to_json for s in fleet]
    )


@groundstation.route('/tasks', methods=['GET'])
def tasks():
    global GROUNDSTATION
    return jsonify(
        status='SUCCESS',
        message='Fetched enqueued tasks.',
        tasks=[task.to_json for task in GROUNDSTATION.tasks]
    )


@groundstation.route('/enqueue', methods=['POST'])
def enqueue():
    """
    Enqueues a task
    """
    form = TaskForm.from_json(request.json)
    if request.method == 'POST' and form.validate():
        resources = form.resources.data
        if resources:
            resources = resources.split(',')

        task = Task(
            name=form.name.data,
            payoff=form.payoff.data,
            resources=tuple(resources)
        )
        GROUNDSTATION.tasks.append(task)
        return jsonify(
            status='SUCCESS',
            message=f'Task {task} enqueued.',
            task=task.to_json
        )

    return jsonify(
        success=True,
        message='Nothing to enqueue or invalid input.'
    )


@groundstation.route('/clear_tasks', methods=['POST'])
def clear_tasks():
    GROUNDSTATION.tasks = list()
    return jsonify(
        status='SUCCESS',
        message='Tasks cleared from groundstation.'
    )


@groundstation.route('/dispatch', methods=['POST'])
def dispatch():
    """
    Dispatch all tasks. This is synchronous.
    """
    tasks = copy.deepcopy(GROUNDSTATION.tasks)
    if not tasks:
        return jsonify({
            'success': True,
            'message': 'No tasks to dispatch.'
        })

    if not GROUNDSTATION.fleet:
        return jsonify({
            'success': True,
            'message': 'No fleet to dispatch to.'
        })
    task_manager = GreedyTaskManager()
    schedules = task_manager.dispatch(
        fleet=GROUNDSTATION.fleet,
        tasks=tasks
    )
    GROUNDSTATION.tasks = list()
    impossible = [s for s in schedules if s.unscheduled]
    possible = [s for s in schedules if not s.unscheduled]
    result = GROUNDSTATION.dispatch(*possible)
    if impossible:
        impossible = impossible.pop()
        GROUNDSTATION.tasks.extend(impossible.tasks)
    return jsonify(
        status='SUCCESS',
        message='Tasks dispatched and executed.',
        scheduled=result,
        unscheduled=[t.to_json for t in impossible.tasks]
    )


@groundstation.route('/logs')
def logs():
    global GROUNDSTATION
    logs = GROUNDSTATION.logs()
    return jsonify(
        status='SUCCESS',
        message='Satellite logs retrieved',
        logs=logs
    )


@groundstation.route('/register/<string:name>/<int:port>')
def register(name, port):
    """
    Launches a new sat process
    """
    global GROUNDSTATION
    sat = Satellite(name=name, port=port)
    GROUNDSTATION.fleet.append(sat)
    return jsonify(
        status='SUCCESS',
        message=f'Satellite {sat} registered in the fleet successfully.',
        satellite=str(sat)
    )
