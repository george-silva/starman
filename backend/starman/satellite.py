# coding: utf-8
import os
import sys
import requests
from datetime import datetime, timezone
from flask import Flask, jsonify, request
from flask_cors import CORS
from .utils import StarmanJSONEncoder, utcnow, random_string
from .models import (
    Satellite,
    Task
)


def get_running_port():
    for a in sys.argv:
        if a.startswith('--port'):
            return int(a.replace('--port=', ''))
    raise ValueError('Satellites should be run using --port= argument.')


def create_app(port=None, failure_rate=0.1):
    SATELLITE_ID = random_string(12)
    if not port:
        port = get_running_port()
    SATELLITE = Satellite(name=SATELLITE_ID, port=port)
    satellite = Flask(__name__)
    CORS(satellite)
    satellite.json_encoder = StarmanJSONEncoder
    satellite.config['TEMPLATES_AUTO_RELOAD'] = True
    satellite.config['JSON_SORT_KEYS'] = False
    try:
        requests.get(f'http://localhost:5000/register/{SATELLITE_ID}/{port}')
    except:
        print('Rogue satellite. No groundstation running.')

    @satellite.route('/')
    def healthcheck():
        """
        returns healthcheck
        """
        return jsonify(
            status='SUCCESS',
            datetime=utcnow(),
            message=f'Satellite {SATELLITE} is alive and well.'
        )

    @satellite.route('/logs')
    def logs():
        return jsonify(
            status='SUCCESS',
            message=f'Fetched logs from {SATELLITE.name}.',
            satellite=SATELLITE.name,
            port=SATELLITE.port,
            logs=[log for log in SATELLITE.logs]
        )

    @satellite.route('/run', methods=['POST'])
    def run():
        data = request.get_json()
        tasks = [
            Task(
                name=item.get('name'),
                payoff=item.get('payoff'),
                resources=item.get('resources')
            ) for item in data
        ]
        if tasks:
            logs = SATELLITE.run(*tasks)
            return jsonify(
                status='SUCCESS',
                message=f'Dispatched tasks were run for {SATELLITE}.',
                satellite=SATELLITE.name,
                port=SATELLITE.port,
                datetime=utcnow(),
                logs=logs
            )
        else:
            return jsonify(
                status='SUCCESS',
                message=f'No tasks were dispatched. Nothing executed.',
                satellite=SATELLITE.name,
                port=SATELLITE.port,
                datetime=utcnow(),
                logs=list()
            )

    return satellite
