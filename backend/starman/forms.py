# coding: utf-8
from wtforms import Form, StringField, IntegerField, BooleanField


class TaskForm(Form):

    name = StringField('name')
    payoff = IntegerField('payoff')
    resources = StringField('resources')
