# coding: utf-8
import string
import random
from flask.json import JSONEncoder
from datetime import datetime, date, timezone


def random_string(size=8):
    return ''.join(random.choice(string.ascii_uppercase) for i in range(size))


def utcnow():
    return datetime.now(timezone.utc)


class StarmanJSONEncoder(JSONEncoder):

    """
    Utility class to encode JSON dates
    properly.
    """

    def default(self, obj):
        try:
            if isinstance(obj, datetime) or isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return super(StarmanJSONEncoder, self).default(self, obj)
