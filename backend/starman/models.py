# coding: utf-8
import copy
import time
import random
import requests
from .utils import utcnow


class Groundstation(object):

    """
    Represents a groundstation
    """

    fleet = None
    tasks = None

    def __init__(self):
        self.fleet = list()
        self.tasks = list()

    def dispatch(self, *schedules):
        return [self._dispatch(s) for s in schedules if s.satellite and s.tasks]

    def logs(self):
        return [self._logs(s) for s in self.fleet]

    def _dispatch(self, schedule):
        """
        Dispatches a single schedule.
        """
        try:
            satellite = schedule.satellite
            url = f'http://localhost:{satellite.port}/run'
            tasks = [t.to_json for t in schedule.tasks]
            response = requests.post(url, json=tasks)
            return response.json()
        except Exception as ex:
            message = getattr(ex, 'message') if hasattr(ex, 'message') else 'UNKNOWN ERROR'
            return {
                'status': 'FAILURE',
                'message': f'Failed to dispatch task to satellite {satellite}. {message}',
                'satellite': satellite.name,
                'port': satellite.port,
                'datetime': utcnow(),
                'logs': list()
            }

    def _logs(self, satellite):
        try:
            url = f'http://localhost:{satellite.port}/logs'
            response = requests.get(url)
            return response.json()
        except Exception as ex:
            message = getattr(ex, 'message') if hasattr(ex, 'message') else 'UNKNOWN ERROR'
            return {
                'status': 'FAILURE',
                'message': f'Failed to get logs from satellite {satellite}. {message}',
                'satellite': satellite.name,
                'port': satellite.port,
                'logs': list()
            }


class Satellite(object):

    """
    Represents a satellite.
    """

    def __init__(self, name, port=5001, failure_rate=0.1):
        self.name = name
        self.resources = list(f'r{x}' for x in range(1, 7))
        self.port = port
        self.failure_rate = failure_rate
        self.logs = list()

    def log_task(self, task, duration, success=True, message=None):
        success = 'SUCCESS' if success else 'FAILURE'
        if not message:
            message = f'TASK {task} was executed with {success}.'
        log = {
            'status': success,
            'message': message,
            'task': task.name,
            'datetime': utcnow(),
            'duration': duration,
            'payoff': task.payoff,
            'resources': task.resources,
        }
        self.logs.append(log)
        return log

    def __str__(self):
        return f'{self.name}:{self.port}'

    def run(self, *tasks):
        """
        Runs multiple tasks and returns the collected logs
        """
        return [self._run(t) for t in tasks]

    def _run(self, task):
        """
        Runs a single task.
        """
        start = time.time()
        try:
            if random.random() <= self.failure_rate:
                error = random_string(4)
                raise ValueError(f'execution failed. {error}')
            print(f'TASK {task} IS BEING EXECUTED.')
            time.sleep(task.duration)
            success = True
            message = f'TASK {task} SUCCEEDED!'
            print(f'TASK {task} SUCCEEDED!')
        except Exception as ex:
            message = getattr(ex.message) if hasattr(ex, 'message') else 'UNKNOWN ERROR'
            print(f'TASK {task} FAILED!')
            success = False
        end = time.time()
        duration = end - start
        return self.log_task(
            task,
            duration=duration,
            success=success,
            message=message
        )

    @property
    def to_json(self):
        """
        Simple method to transform a satellite in a JSON
        like resource
        """
        return {
            'name': self.name,
            'port': self.port,
            'address': str(self)
        }


class Task(object):

    """represents a task"""

    name = None
    resources = None
    payoff = 1
    execute_at = None

    @property
    def to_json(self):
        """
        Simple method to transform a task in a JSON
        like resource
        """
        return {
            'name': self.name,
            'resources': self.resources,
            'payoff': self.payoff
        }

    def __init__(self, name, resources, payoff=1, execute_at=None):

        if not name:
            raise ValueError('name is mandatory')
        self.name = name
        self.resources = tuple(resources)
        self.payoff = payoff
        self.duration = random.randint(1, 10)
        self.execute_at = execute_at

    def __lt__(self, other):
        """
        Implemented to guarantee ordering.
        """
        if self.payoff < other.payoff:
            return True
        if self.payoff == other.payoff:
            return len(self.resources) > len(other.resources)
        return False

    def __repr__(self):
        return f"Task(name='{self.name}', resources={self.resources}, payoff={self.payoff})"

    def __str__(self):
        return f'{self.name} - {self.payoff}'


class Schedule(object):

    def __init__(self, satellite=None, tasks=None):
        self.satellite = satellite
        if not tasks:
            tasks = list()
        self.tasks = tasks

    def __str__(self):
        return self.name

    @property
    def to_json(self):
        """
        Simple method to transform a schedule in a JSON
        like resource
        """
        return {
            'name': str(self.satellite) if self.satellite else 'UNSCHEDULED',
            'type': 'IMMEDIATE',
            'tasks': [
                task.to_json for task in self.tasks
            ]
        }

    @property
    def unscheduled(self):
        return self.satellite is None

    def used_resources(self):
        resources = list()
        for t in self.tasks:
            resources.extend(list(t.resources))

        return set(resources)

    def can_schedule(self, task):
        new_resources = set(task.resources)
        if self.satellite:
            available_resources = set(self.satellite.resources)
        else:
            available_resources = set()
        # makes sure that this sat has all the resources
        # we need for executing this task
        if not new_resources <= available_resources:
            return False

        used_resources = self.used_resources()
        conflicting = new_resources & used_resources

        # makes sure that all the resources we're using
        # are not already being used
        if conflicting:
            return False
        return True

    def schedule(self, task):
        self.tasks.append(task)
