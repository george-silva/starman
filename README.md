# starman

Starman is an exercise provided by Satellogic.

The idea is to have control of a ground station
and N satellites and establish communication
between these nodes.

Each node is a simple Flask application. 

The groundstation **must** run by default on port `5000`.
Each satellite will take a port sequentially, like `5001`, `5002`,
etc.

This exercise does not use any database, for the sake of simplicity.
If your `groundstation` process dies or restart, you'll need to restart the
satellite processes as well.

There are two ways of interacting with the application:

1. Using the CLI tools provided, developed using `requests` and `click`;
1. Using the Vue application developed using `axios` and `tailwindcss`;

![screenshot](docs/screenshot.png)

## Tech stack

1. Python 3.6
1. Flask
1. Click
1. Vue JS

### Packages used

For the backend:

1. Flask (HTTP Server)
1. Flask-CORS (enabling CORS requests)
1. Flask-WTF (forms and form validation)
1. WTForms-JSON (adaptor to work with JSON in the forms)
1. click (cli)
1. colorama (cli)

For the front-end

1. Vue JS
1. TailwindCSS (utility CSS framework)
1. Axios (for HTTP requests)
1. Vuex (state management)
1. vue-json-tree-view (to implement the `terminal` part)

## Considerations and Improvements

These are some considerations regarding some of the development choices I've made.

This was coded all together in a single repository, we could have separated
the `frontend` and `backend` folders in separate repositories. They could be more
modular and it would be easier when working on a team;

While I was developing the `TaskManager` algorithm I was investigating the possibility
of using Google's `or-tools` or `python-constraint` as a way to solve the problem
posed by the exercise. While this is somewhat like the knapsack problem, when you add
more variables, for example, length, payoff, energy requirements, I'm pretty sure
that a more scientific approach (with `or-tools`)  is required.

To be honest, I was not able to get a working solution with `or-tools` in time.

The unit tests were created only for the `heavy` parts of the application. That is:
`models.py` and `tasks.py`. I did not created any unit tests for the servers because they are very
lean. The total test coverage may seem low, but it's high on the important files (72% for `models.py`
and 97% for `tasks.py`).

There is some thin line here between fat models and services. Most of the times
I've used the models to do some heavy lifting, like `groundstation` querying for 
all `satellite` logs. This perhaps would be a best fit for a service layer.

The only "service" implemented was `TaskManager` responsible to create new schedule
objects.

This implementation *tries* really hard to be pure in the functional programming sense.
I avoided mutating objects the best I could. I'm pretty sure there are other places
that can be improved in this regard. Avoiding mutation is a great way to avoid bugs.

The task running was implemented directly in the `Satellite` class. While this
works, as I've said before, I would try to go to the `Service` route.

Since the task running part was implemented synchronously the `groundstation` keeps
waiting for the responses of all satellites. I know that this is not the best case scenario.
Ideally this should be implemented with async tools, like so:

1. User adds several tasks, to be dispatched;
1. User hits the dispatch button;
1. TaskManager (with or-tools possibily) creates a new schedule associating
satellites and tasks;
1. Groundstation API fires the requests to each satellite;
1. Each satellite enqueues the task for execution (immediate or scheduled in a later date)
**and acknowledges** the request, sending back the queued tasks;
1. Tasks are run async with the `dispatch` request from the user;

At last, but not least: I probably would NOT code this in Flask in real
life. I did this in Flask because it was easy to get it running and it
was easy to distribute, so you could test it.

In real life I've would used Django for the Groundstation. For each
satellite (which I guess they DO have a server on board) I would choose
a leaner server - perhaps even Flask.

## [Getting Started](docs/getting-started.md)

## [CLI](docs/cli.md)

## [API](docs/api.md)
